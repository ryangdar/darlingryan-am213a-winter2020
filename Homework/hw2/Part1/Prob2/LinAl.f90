module LinAl

implicit none
  
contains

  subroutine readMat(mat,msize,nsize,filename,fileUnit)

    implicit none
    CHARACTER(LEN=*), INTENT(IN) :: filename
    REAL, DIMENSION(:,:), ALLOCATABLE, INTENT(INOUT) :: mat
    INTEGER, INTENT(IN) :: fileUnit
    INTEGER, INTENT(OUT) :: msize, nsize 
    INTEGER :: i,j

    ! Reads a file containing the matrix A 
    ! Sample file:
    !
    ! 4 4 
    ! 2.0 1.0 1.0 0.0
    ! 4.0 3.0 3.0 1.0
    ! 8.0 7.0 9.0 5.0
    ! 6.0 7.0 9.0 8.0
    !
    ! Note that the first 2 numbers in the first line are the matrix dimensions, i.e., 4x4,
    ! then the next msize lines are the matrix entries. This matrix is found in Eq. 2.18 of the lecture note.
    ! Note that entries must be separated by a tab.


   OPEN( UNIT = fileUnit, FILE = filename, STATUS = 'old', ACTION = 'read' )
     
   WRITE(*,*) msize
   READ(fileUnit,*) msize,nsize
   ALLOCATE(mat(msize,nsize))
    
   DO i=1,msize

      READ(fileUnit,*) ( mat(i,j), j=1,nsize )

   END DO

   CLOSE(fileUnit)
   
   END SUBROUTINE readMat
   
   FUNCTION LA_trace(mat, msize)
   
   IMPLICIT NONE
   INTEGER, INTENT(IN) :: msize
   REAL, DIMENSION(msize,msize), INTENT(IN) :: mat
   REAL :: LA_trace
   INTEGER :: i


   LA_trace = 0.

   DO i = 1, msize

   	LA_trace = LA_trace + mat(i,i)

   END DO

   END FUNCTION LA_trace

   FUNCTION LA_twoNorm(vector, msize)
   INTEGER, INTENT(IN) :: msize
   REAL, DIMENSION(msize), INTENT(IN) :: vector
   REAL :: LA_twoNorm

   INTEGER :: i

   LA_twoNorm = 0.
   DO i = 1, msize

	LA_twoNorm = LA_twoNorm + vector(i)**2

   END DO
   LA_twoNorm = SQRT(LA_twoNorm)

   END FUNCTION LA_twoNorm

  SUBROUTINE LA_writeMatrixToScreen( Matrix, msize, nsize )
  
  IMPLICIT NONE
  REAL, DIMENSION(msize,msize), INTENT(IN) :: Matrix
  INTEGER, INTENT(IN) :: msize, nsize
  INTEGER :: i,j

  DO i = 1, msize

	WRITE(*,*) ( Matrix(i,j) , j = 1, nsize )

  END DO
  WRITE(*,*) " "

  END SUBROUTINE LA_writeMatrixToScreen

  SUBROUTINE LA_GaussElimPiv(A, B, msize, psize, iSing)

  IMPLICIT NONE
  INTEGER, INTENT(IN) :: msize, psize
  REAL, DIMENSION( msize, msize ), INTENT(INOUT) :: A
  REAL, DIMENSION( msize, psize ), INTENT(INOUT) :: B
  LOGICAL, INTENT(INOUT) :: iSing
  REAL, DIMENSION( msize, msize ) :: Ac
  REAL, DIMENSION( msize, psize ) :: Bc
  REAL :: maximum
  REAL :: temp
  INTEGER :: i,j,k

  Ac = A
  Bc = B

  DO j = 1, (msize - 1)

	maximum = MAXVAL(ABS(Ac(j:msize,j)))

	IF( ABS( Ac(j,j) ) .NE. maximum ) THEN

		DO k = j+1, msize

			IF( ABS(Ac(k,j)) == maximum ) THEN

				Ac = A
				A(j,j:msize) = A(k,j:msize)
				A(k,j:msize) = Ac(j,j:msize)
				Bc = B
				B(j,:) = B(k,:)
				B(k,:) = Bc(j,:)

				EXIT
		
			END IF
		
		END DO

	END IF

	IF( ABS(A(j,j)) < EPSILON(0.) ) THEN

		WRITE(*,*) "The matrix is singular"
		iSing = .TRUE.
		EXIT

	ELSE
	
		DO i = (j+1), msize

			temp = A(i,j) / A(j,j)
			A(i,:) = A(i,:) - temp * A(j,:)
			B(i,:) = B(i,:) - temp * B(j,:)	
	
		END DO

	END IF

  END DO

  iSing = .FALSE.

  IF( .NOT. iSing ) THEN

	  WRITE(*,*) "Matrix A after Gaussian Elimination:"
	  CALL LA_writeMatrixToScreen(A,msize,msize)
	  WRITE(*,*) "Matrix B after Gaussian Elimination:"
	  CALL LA_writeMatrixToScreen(B,msize,psize)

  END IF

  END SUBROUTINE LA_GaussElimPiv

  SUBROUTINE LA_BackwardSolve(U,y,x,msize)
  
  IMPLICIT NONE
  INTEGER :: i
  INTEGER :: j
  REAL :: summ
  
  REAL, INTENT(IN), DIMENSION(msize,msize) :: u
  REAL, INTENT(OUT), DIMENSION(msize) :: x
  REAL, INTENT(IN), DIMENSION(msize) :: y
  INTEGER, INTENT(IN) :: msize	

  x = y;  

  DO i =msize,1,-1 
	summ = 0.
	DO j = (i+1),msize 
		summ = summ + u(i,j)*x(j)
  	END DO 
  	x(i) = (y(i) - summ)/u(i,i)
  END DO 

  END SUBROUTINE LA_BackwardSolve


  SUBROUTINE LA_luDecomp(A,s,msize,iSing)

  IMPLICIT NONE
  INTEGER, INTENT(IN) :: msize
  REAL, INTENT(INOUT), DIMENSION(msize,msize) :: A
  INTEGER, DIMENSION(msize), INTENT(OUT) :: s
  LOGICAL, INTENT(OUT) :: iSing
  INTEGER :: i
  INTEGER :: j
  INTEGER :: sub_index
  REAL 	:: maximum
  INTEGER :: k
  REAL, DIMENSION(msize,msize) :: Ac
  INTEGER, DIMENSION(msize) :: sc

  Ac = A
  DO i = 1,msize
	s(i) = i
  END DO
  sc = s

  iSing = .FALSE.

  sub_index = 1

  DO j = 1,msize        

	maximum = MAXVAL(ABS(A(j:msize,j)))
	DO i = (j+1),msize 

		IF(ABS(A(i,j))==maximum) THEN

			Ac = A
			A(j,:) = A(i,:)
			A(i,:) = Ac(j,:) 
			
			sc = s
			s(j) = s(i)
			s(i) = sc(j)
			EXIT 
		END IF

		IF( ABS(A(j,j)) < EPSILON(0.) ) THEN

			WRITE(*,*) "The matrix A is singular"
			EXIT

		END IF
		
	END DO 
	
	IF( .NOT. iSing ) THEN

		DO i = (j+1), msize

			A(i,j) = A(i,j) / A(j,j)
			DO k = (j+1), msize 

				A(i,k) = A(i,k) - A(i,j) * A(j,k)

			END DO  

		END DO

	ELSE

		EXIT

	END IF

  END DO 

  IF( .NOT. iSing ) THEN
  
	WRITE(*,*) " "
	WRITE(*,*) "The matrix A after LU decomp"
 	CALL LA_writeMatrixToScreen(A,msize,msize)

	WRITE(*,*) " "
	WRITE(*,*) "The vector s is"
	DO i = 1, msize

		WRITE(*,*) s(i)

	END DO
	WRITE(*,*) " "
  END IF

  END SUBROUTINE LA_luDecomp

  SUBROUTINE LA_luBackSub(A,B,X,s,msize)
  
  IMPLICIT NONE
  INTEGER, INTENT(IN) :: msize
  INTEGER, DIMENSION(msize), INTENT(IN) :: s
  REAL, DIMENSION(msize,msize), INTENT(IN) :: A
  REAL, DIMENSION(msize), INTENT(INOUT) :: B
  REAL, DIMENSION(msize), INTENT(INOUT) :: X
  INTEGER :: i,j,k
  INTEGER :: minimum
  REAL, DIMENSION(msize,msize) :: Ac
  REAL, DIMENSION(msize) :: Bc, Xc, Y
  INTEGER, DIMENSION(msize) :: sc, scc
  REAL :: summ

  DO i = 1, msize

	scc(i) = i 

  END DO
  sc = scc
  Y = B
  DO j = 1, msize

	DO i = j, msize

		IF( sc(i) == s(j) ) THEN

			Bc = Y
			Y(j) = Y(i)
			Y(i) = Bc(j)
			scc = sc
			sc(j) = sc(i)
			sc(i) = scc(j)
			EXIT			

		END IF

	END DO

  END DO

  DO j = 1, msize - 1

	DO i = j+1, msize

		Y(i) = Y(i) - Y(j)*A(i,j)	

	END DO

  END DO

  X = Y
  DO i = msize,1,-1

	summ = 0.
	DO k = i+1, msize

		summ = summ + A(i,k)*X(k)

	END DO
	X(i) = (Y(i) - summ) / A(i,i)

  END DO

  END SUBROUTINE

end module LinAl
