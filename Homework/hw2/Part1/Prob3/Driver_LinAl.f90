Program Driver_LinAl

USE LinAl

IMPLICIT NONE
REAL, DIMENSION(:,:), ALLOCATABLE :: A, As
REAL, DIMENSION(:,:), ALLOCATABLE :: B, Bs
REAL, DIMENSION(:,:), ALLOCATABLE :: ErrMat
REAL, DIMENSION(:,:), ALLOCATABLE :: X
INTEGER, DIMENSION(:), ALLOCATABLE :: s
INTEGER :: msize, nsize, psize
LOGICAL :: iSing
INTEGER :: i

CALL readMat( A , msize , nsize , "Amat.dat", 1)
CALL readMat( B , msize, psize  , "Bmat.dat", 11 )
	
WRITE(*,*) "Matrix A:"
CALL LA_writeMatrixToScreen( A, msize, msize )
WRITE(*,*) "Matrix B:"
CALL LA_writeMatrixToScreen( B, msize, psize )

ALLOCATE( As( msize, msize ) )
ALLOCATE( X( msize, psize ) )
ALLOCATE( ErrMat( msize, psize ) )
ALLOCATE( s(msize) )
As = A; Bs = B;
X = 0.; ErrMat = 100.0

CALL LA_luDecomp( A, s, msize, iSing )

IF( .NOT. iSing ) THEN

    DO i = 1, psize

        CALL LA_luBackSub( A, B(:,i), X(:,i),s, msize )

    END DO 
    WRITE(*,*) "X matrix:"
    CALL LA_writeMatrixToScreen( X, msize, psize )


    WRITE(*,*) "AX - B:"		
    ErrMat = MATMUL(As,X) - B
    CALL LA_writeMatrixToScreen( ErrMat, msize, psize )
    WRITE(*,*) "Norms:"
    WRITE(*,*) ( LA_twoNorm(ErrMat(:,i),psize), i = 1, psize )


END IF

DEALLOCATE(A)
DEALLOCATE(X)
DEALLOCATE(B)

End Program Driver_LinAl
