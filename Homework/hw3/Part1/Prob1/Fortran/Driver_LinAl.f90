Program Driver_LinAl

USE LinAl

IMPLICIT NONE
REAL, DIMENSION(:,:), ALLOCATABLE :: A
INTEGER :: msize, nsize
INTEGER :: i

CALL readMat( A , msize , nsize , "Amat.dat", 1)

WRITE(*,*) "Matrix trace: "
WRITE(*,*) " "
WRITE(*,*) LA_trace(A,msize)
WRITE(*,*) " "
WRITE(*,*) "Norms:"
WRITE(*,*) ( LA_twoNorm(A(:,i),msize), i = 1, msize )
WRITE(*,*) " "
WRITE(*,*) "Print Matrix: "
WRITE(*,*) "A = ... "
CALL LA_writeMatrixToScreen( A, msize, nsize )

End Program Driver_LinAl
