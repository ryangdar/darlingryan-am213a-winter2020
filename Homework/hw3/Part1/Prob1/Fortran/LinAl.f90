module LinAl

implicit none
  
contains

  subroutine readMat(mat,msize,nsize,filename,fileUnit)

    implicit none
    CHARACTER(LEN=*), INTENT(IN) :: filename
    REAL, DIMENSION(:,:), ALLOCATABLE, INTENT(INOUT) :: mat
    INTEGER, INTENT(IN) :: fileUnit
    INTEGER, INTENT(OUT) :: msize, nsize 
    INTEGER :: i,j

    ! Reads a file containing the matrix A 
    ! Sample file:
    !
    ! 4 4 
    ! 2.0 1.0 1.0 0.0
    ! 4.0 3.0 3.0 1.0
    ! 8.0 7.0 9.0 5.0
    ! 6.0 7.0 9.0 8.0
    !
    ! Note that the first 2 numbers in the first line are the matrix dimensions, i.e., 4x4,
    ! then the next msize lines are the matrix entries. This matrix is found in Eq. 2.18 of the lecture note.
    ! Note that entries must be separated by a tab.


   OPEN( UNIT = fileUnit, FILE = filename, STATUS = 'old', ACTION = 'read' )
     
   WRITE(*,*) msize
   READ(fileUnit,*) msize,nsize
   ALLOCATE(mat(msize,nsize))
    
   DO i=1,msize

      READ(fileUnit,*) ( mat(i,j), j=1,nsize )

   END DO

   CLOSE(fileUnit)
   
   END SUBROUTINE readMat
   
   FUNCTION LA_trace(mat, msize)

   IMPLICIT NONE

   INTEGER, INTENT(IN) :: msize
   REAL, DIMENSION(msize,msize), INTENT(IN) :: mat
   REAL :: LA_trace

   INTEGER :: i


   LA_trace = 0.

   DO i = 1, msize

        LA_trace = LA_trace + mat(i,i)

   END DO

   END FUNCTION LA_trace

   FUNCTION LA_twoNorm(vector, msize)
    INTEGER, INTENT(IN) :: msize
    REAL, DIMENSION(msize), INTENT(IN) :: vector
    REAL :: LA_twoNorm
    INTEGER :: i

    LA_twoNorm = 0.
    DO i = 1, msize

        LA_twoNorm = LA_twoNorm + vector(i)**2

    END DO
    LA_twoNorm = SQRT(LA_twoNorm)

   END FUNCTION LA_twoNorm

  SUBROUTINE LA_writeMatrixToScreen( Matrix, msize, nsize )

    IMPLICIT NONE
    REAL, DIMENSION(msize,msize), INTENT(IN) :: Matrix
    INTEGER, INTENT(IN) :: msize, nsize
    INTEGER :: i,j

    DO i = 1, msize

        WRITE(*,*) ( Matrix(i,j) , j = 1, nsize )

    END DO
    WRITE(*,*) " "

  END SUBROUTINE LA_writeMatrixToScreen




end module LinAl
