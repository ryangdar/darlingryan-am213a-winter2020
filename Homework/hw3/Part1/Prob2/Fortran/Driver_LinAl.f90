Program Driver_LinAl

USE LinAl

IMPLICIT NONE
REAL, DIMENSION(:,:), ALLOCATABLE :: A, R, Q
REAL, DIMENSION(:), ALLOCATABLE :: C, D
INTEGER :: msize, nsize,n , np
INTEGER :: i,j

LOGICAL :: sing

CALL readMat( A , msize , nsize , "atkinson.dat", 1)

ALLOCATE(C(msize))
Allocate(D(msize))
Allocate(R(msize, msize))
Allocate(Q(msize, msize))

WRITE(*,*) "Solution vector X: "
WRITE(*,*) " "
WRITE(*,*) LA_trace(A,msize)
WRITE(*,*) " "
WRITE(*,*) "Norms:"
WRITE(*,*) ( LA_twoNorm(A(:,i),msize), i = 1, msize )
WRITE(*,*) " "
WRITE(*,*) "Print Matrix: "
WRITE(*,*) "A = ... "
CALL LA_writeMatrixToScreen( A, msize, nsize )
CALL  qrdcmp(a,msize,msize,c,d,sing)
WRITE(*,*) " "


DO i = 1, msize
    Do j=1, msize
        R(i,j)=0
        Q(i,j)=0
    Enddo
END DO

DO i = 1, msize
    R(i,i) = D(i)
    Do j=i+1, msize
        R(i,j) = A(i,j)
    Enddo
END DO

WRITE(*,*) " "
WRITE(*,*) "Q = ... "
CALL LA_writeMatrixToScreen(Q, msize, nsize)

WRITE(*,*) " "
WRITE(*,*) "R = ... "
CALL LA_writeMatrixToScreen(R, msize, nsize)

End Program Driver_LinAl
