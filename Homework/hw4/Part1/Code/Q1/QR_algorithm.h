#ifndef _QR_ALGORITHM_H_
#define _QR_ALGORITHM_H_

#define QR_SUCCESS						0
#define	QR_ERROR_MATRIX_NOT_SQUARE		-1

int QR_algorithm(matrix* A, double_vect*& out_eigenvalues, double_vect*& out_convergence_measure);
int QR_algorithm_shift(matrix* A, double_vect*& out_eigenvalues, double_vect*& out_convergence_measure);

#endif
