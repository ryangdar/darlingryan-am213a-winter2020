#include <cstdio>
#include <cstdlib>
#include <math.h>

#include "matrix.h"
#include "double_vect.h"
#include "QR_algorithm.h"
#include "QR_algorithm_test.h"

#define N	3

int QR_algorithm_test()
{
	puts("::::::::::::: QR Alogrigthm :::::::::::::\n");

	matrix* A = matrix_new(N, N);

	double in[N][N] =
	//{
	//	{ 12, -51,   4 },
	//	{  6, 167, -68 },
	//	{ -4,  24, -41 },
	//};

	{
		{ 3, 1, 0 },
		{ 1, 2, 1 },
		{ 0, 1, 1 },
	};

	for (int i = 0; i < A->m; i++)
		for (int j = 0; j < A->n; j++)
			A->v[i][j] = in[i][j];

	puts("Input Matrix A = ");
	matrix_print(A);

	double_vect* eigenvalues;
	double_vect* convergence_measure;

	int result = QR_algorithm(A, eigenvalues, convergence_measure);

	if (result == QR_SUCCESS)
	{
		puts("Eigen values:");

		for (int i = 0; i < eigenvalues->length; i++)
		{
			printf("\t%f\n", double_vect_get(eigenvalues, i));
		}
	}

	return result;
}

int QR_algorithm_shift_test()
{
	puts("::::::::::::: QR Alogrigthm Shift :::::::::::::\n");
	matrix* A = matrix_new(N, N);

	double in[N][N] =
	//{
	//	{ 12, -51,   4 },
	//	{  6, 167, -68 },
	//	{ -4,  24, -41 },
	//};

	{
		{ 3, 1, 0 },
		{ 1, 2, 1 },
		{ 0, 1, 1 },
	};

	for (int i = 0; i < A->m; i++)
		for (int j = 0; j < A->n; j++)
			A->v[i][j] = in[i][j];

	puts("Input Matrix A = ");
	matrix_print(A);

	double_vect* eigenvalues;
	double_vect* convergence_measure;

	int result = QR_algorithm_shift(A, eigenvalues, convergence_measure);

	if (result == QR_SUCCESS)
	{
		puts("Eigen values:");

		for (int i = 0; i < eigenvalues->length; i++)
		{
			printf("\t%f\n", double_vect_get(eigenvalues, i));
		}
	}

	return result;
}
