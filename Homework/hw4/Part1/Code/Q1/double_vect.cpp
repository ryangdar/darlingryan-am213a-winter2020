#include <cstdlib>

#include "double_vect.h"

#define CAPACITY		500

double_vect* double_vect_new()
{
	double_vect* vect = (double_vect*)malloc(sizeof(double_vect));
	vect->values = (double*)malloc(CAPACITY * sizeof(double));

	for (int i = 0; i < CAPACITY; i++)
	{
		vect->values[i] = 0;
	}

	vect->length = 0;
	return vect;
}

double double_vect_append(double_vect* vect, double value) 
{
	if (vect->length < CAPACITY)
	{
		vect->values[vect->length] = value;
		vect->length++;
	}
	else 
	{
		//Buffer is full
		exit(-1);
	}
}

double double_vect_get(double_vect* vect, int index)
{
	if (index < 0)
	{
		return vect->values[vect->length + index];
	}
	else 
	{
		return vect->values[index];
	}
}

void double_vect_insert(double_vect* vect, int index, double value)
{
	if (index >= 0)
	{
		vect->values[index] = value;
		if (index >= vect->length)
		{
			vect->length = index + 1;
		}
	}
}

int double_vect_length(double_vect* vect)
{
	return vect->length;
}