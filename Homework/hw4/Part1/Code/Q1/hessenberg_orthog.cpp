////////////////////////////////////////////////////////////////////////////////
// File: hessenberg_orthog.c                                                  //
// Routine(s):                                                                //
//    Hessenberg_Form_Orthogonal                                              //
////////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>                      // required for malloc() and free()    
#include <math.h>                        // required for sqrt()

static void Identity_Matrix(double* A, int n)
{
    int i, j;

    for (i = 0; i < n - 1; i++) {
        *A++ = 1.0;
        for (j = 0; j < n; j++) *A++ = 0.0;
    }

    *A = 1.0;
}


int Hessenberg_Form_Orthogonal(double* A, double* U, int n)
{
    int i, k, col;
    double* u;
    double* p_row, * psubdiag;
    double* pA, * pU;
    double sss;                             // signed sqrt of sum of squares
    double scale;
    double innerproduct;

    // n x n matrices for which n <= 2 are already in Hessenberg form

    Identity_Matrix(U, n);
    if (n <= 2)
        return 0;

    // Reserve auxillary storage, if unavailable, return an error

    u = (double*)malloc(n * sizeof(double));
    if (u == NULL) return -1;

    // For each column use a Householder transformation 
    //   to zero all entries below the subdiagonal.

    for (psubdiag = A + n, col = 0; col < (n - 2); psubdiag += (n + 1), col++) {

        // Calculate the signed square root of the sum of squares of the
        // elements below the diagonal.

        for (pA = psubdiag, sss = 0.0, i = col + 1; i < n; pA += n, i++)
            sss += *pA * *pA;
        if (sss == 0.0) continue;
        sss = sqrt(sss);
        if (*psubdiag >= 0.0) sss = -sss;

        // Calculate the Householder transformation Q = I - 2uu'/u'u.

        u[col + 1] = *psubdiag - sss;
        *psubdiag = sss;
        for (pA = psubdiag + n, i = col + 2; i < n; pA += n, i++) {
            u[i] = *pA;
            *pA = 0.0;
        }

        // Premultiply A by Q

        scale = -1.0 / (sss * u[col + 1]);
        for (p_row = psubdiag - col, i = col + 1; i < n; i++) {
            pA = A + n * (col + 1) + i;
            for (innerproduct = 0.0, k = col + 1; k < n; pA += n, k++)
                innerproduct += u[k] * *pA;
            innerproduct *= scale;
            for (pA = p_row + i, k = col + 1; k < n; pA += n, k++)
                *pA -= u[k] * innerproduct;
        }

        // Postmultiply QA by Q

        for (p_row = A, i = 0; i < n; p_row += n, i++) {
            for (innerproduct = 0.0, k = col + 1; k < n; k++)
                innerproduct += u[k] * *(p_row + k);
            innerproduct *= scale;
            for (k = col + 1; k < n; k++)
                *(p_row + k) -= u[k] * innerproduct;
        }

        // Postmultiply U by (I - 2uu')

        for (i = 0, pU = U; i < n; pU += n, i++) {
            for (innerproduct = 0.0, k = col + 1; k < n; k++)
                innerproduct += u[k] * *(pU + k);
            innerproduct *= scale;
            for (k = col + 1; k < n; k++)
                *(pU + k) -= u[k] * innerproduct;
        }

    }

    free(u);

    return 0;
}
