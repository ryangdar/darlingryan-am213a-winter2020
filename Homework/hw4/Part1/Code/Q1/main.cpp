#include <cstdio>
#include <cstdlib>
#include <math.h>

#include "matrix.h"
#include "double_vect.h"
#include "QR_algorithm_test.h"

#include "double_vect.h"
#include "matrix.h"

#define N	4

int main()
{
	//QR_algorithm_test();
	//
	//puts("\n");
	//
	//QR_algorithm_shift_test();

	//Testing the matrix_tridiag() function

	matrix* A = matrix_new(N, N);

	double in[N][N] =
	{
		{ 5.0, 4.0, 1.0, 1.0 },
		{ 4.0, 5.0, 1.0, 1.0 },
		{ 1.0, 1.0, 4.0, 2.0 },
		{ 1.0, 1.0, 2.0, 4.0 }
	};

	for (int i = 0; i < A->m; i++)
		for (int j = 0; j < A->n; j++)
			A->v[i][j] = in[i][j];

	puts("Input Matrix A = \n");
	matrix_print(A);
	puts("\n");

	puts("Tridiagonal Reduced Matrix = ");
	matrix* tridiag = matrix_tridiag(A);
	matrix_print(tridiag);

	getchar();
	return 0;
}