#include "matrix.h"

#include <cstdio>
#include <cstdlib>
#include <math.h>

#include "hessenberg_orthog.h"

matrix* matrix_new(int m, int n)
{
	matrix* x = (matrix*)malloc(sizeof(matrix));

	x->v = (double**)malloc(sizeof(double*) * m);
	x->v[0] = (double*)calloc(sizeof(double), m * n);
	
	for (int i = 0; i < m; i++)
		x->v[i] = x->v[0] + n * i;

	x->m = m;
	x->n = n;

	for (int i = 0; i < x->m; i++)
	{
		for (int j = 0; j < x->n; j++)
		{
			x->v[i][j] = 0.0;
		}
	}

	return x;
}

void matrix_delete(matrix* m)
{
	free(m->v[0]);
	free(m->v);
	free(m);
}

matrix* matrix_transpose(matrix* mat)
{
	matrix* trans = matrix_copy(mat);
	for (int i = 0; i < trans->m; i++) 
	{
		for (int j = 0; j < i; j++) 
		{
			double t = trans->v[i][j];
			trans->v[i][j] = trans->v[j][i];
			trans->v[j][i] = t;
		}
	}

	int t = trans->m;
	trans->m = trans->n;
	trans->n = t;

	return trans;
}

double matrix_get(matrix* mat, int m, int n)
{
	return mat->v[m][n];
}

void matrix_set(matrix* mat, int m, int n, double value)
{
	mat->v[m][n] = value;
}

int matrix_length(matrix* mat)
{
	if (mat->m > mat->n)
		return mat->m;
	else 
		return mat->n;
}

matrix* matrix_add(matrix* mat1, matrix* mat2)
{
	matrix* result = matrix_new(mat1->m, mat1->n);
	for (int i = 0; i < result->m; i++)
	{
		for (int j = 0; j < result->n; j++)
		{
			result->v[i][j] = mat1->v[i][j] + mat2->v[i][j];
		}
	}

	return result;
}

matrix* matrix_add3(matrix* mat1, matrix* mat2, matrix* mat3)
{
	matrix* result = matrix_new(mat1->m, mat1->n);
	for (int i = 0; i < result->m; i++)
	{
		for (int j = 0; j < result->n; j++)
		{
			result->v[i][j] = mat1->v[i][j] + mat2->v[i][j] + mat3->v[i][j];
		}
	}

	return result;
}

matrix* matrix_subtract(matrix* mat1, matrix* mat2)
{
	matrix* result = matrix_new(mat1->m, mat1->n);
	for (int i = 0; i < result->m; i++)
	{
		for (int j = 0; j < result->n; j++)
		{
			result->v[i][j] = mat1->v[i][j] - mat2->v[i][j];
		}
	}

	return result;
}

matrix* matrix_copy(matrix* mat)
{
	matrix* x = matrix_new(mat->m, mat->n);

	for (int i = 0; i < mat->m; i++)
	{
		for (int j = 0; j < mat->n; j++)
		{
			x->v[i][j] = mat->v[i][j];
		}
	}
	
	return x;
}

matrix* matrix_slice(matrix* mat, int slice_m, int slice_n)
{
	matrix *slice = matrix_new(slice_m, slice_n);
	for (int i = 0; i < slice->m; i++)
	{
		for (int j = 0; j < slice->n; j++)
		{
			slice->v[i][j];
		}
	}

	return slice;
}

matrix* matrix_mul(matrix* x, matrix* y)
{
	if (x->n != y->m)
		return NULL;

	matrix* r = matrix_new(x->m, y->n);

	for (int i = 0; i < x->m; i++)
		for (int j = 0; j < y->n; j++)
			for (int k = 0; k < x->n; k++)
				r->v[i][j] += x->v[i][k] * y->v[k][j];

	return r;
}

void matrix_print(matrix* m)
{
	for (int i = 0; i < m->m; i++) {
		for (int j = 0; j < m->n; j++) {
			printf(" %8.4f", m->v[i][j]);
		}
		printf("\n");
	}
	printf("\n");
}

void matrix_hessenberg(matrix* mat)
{
	matrix* u = matrix_new(mat->m, mat->n);
	Hessenberg_Form_Orthogonal(mat->v[0], u->v[0], mat->m);
}

static void matrix_copy_column(matrix* src, int srcColIndex, matrix* dst, int dstColIndex)
{
	for (int i = 0; i < dst->m; i++)
	{
		dst -> v[i][dstColIndex] = src -> v[i][srcColIndex];
	}
}

static void matrix_column_subtract(matrix* m1, int c1, matrix* m2, int c2)
{
	for (int i = 0; i < m1->m; i++)
	{
		m1->v[i][c1] -= m2->v[i][c2];
	}
}

/* Multiplies the matrix column c in m by k */
static matrix* matrix_column_multiply(matrix* m, int c, double k)
{
	for (int i = 0; i < m->m; i++) 
	{
		m->v[i][c] *= k;
	}
	
	return m;
}

/* Returns the length of the vector column in m */
static double vector_length(matrix* m, int column) 
{
	double length = 0;
	for (int row = 0; row < m->m; row++) 
	{
		length += m->v[row][column] * m->v[row][column];
	}

	return sqrt(length);
}

/* Divides the matrix column c in m by k */
static matrix* matrix_column_divide(matrix* m, int c, double k) 
{
	for (int i = 0; i < m->m; i++) 
	{
		m->v[i][c] /= k;
	}

	return m;
}

void matrix_qr_factorization(matrix* A, matrix* Q, matrix* R)
{
	matrix* T = matrix_new(A->m, 1);
	matrix* S = matrix_new(A->m, 1);

	for (int i = 0; i < A->n; i++) 
	{

		matrix_copy_column(A, i, Q, i);

		for (int j = 0; j < i; j++) {

			matrix_copy_column(Q, j, T, 0);
			matrix_copy_column(A, i, S, 0);

			double r = 0;
			
			for (int k = 0; k < A->m; k++) 
			{
				r += T->v[k][0] * S->v[k][0];
			}

			R->v[j][i] = r;
			matrix_column_subtract(Q, i, matrix_column_multiply(T, 0, r), 0);

		}

		R->v[i][i] = vector_length(Q, i);
		matrix_column_divide(Q, i, R->v[i][i]);

	}

}

int matrix_is_zero(matrix* mat)
{
	for (int i = 0; i < mat->m; i++)
	{
		for (int j = 0; j < mat->n; j++)
		{
			if (fabs(mat->v[i][j] > 0.000000000000001))
			{
				return 0;
			}
		}
	}

	return 1;
}

matrix* matrix_mul_scalar(matrix* mat, double k)
{
	matrix* result = matrix_new(mat->m, mat->n);

	for (int i = 0; i < mat->m; i++)
	{
		for (int j = 0; j < mat->n; j++)
		{
			result->v[i][j] = mat->v[i][j] * k;
		}
	}

	return result;
}

matrix* matrix_eye(int m)
{
	matrix* mat = matrix_new(m, m);
	for (int i = 0; i < m; i++)
	{
		for (int j = 0; j < m; j++)
		{
			if (i == j)
			{
				mat->v[i][j] = 1;
			}
			else
			{
				mat->v[i][j] = 0;
			}
		}
	}

	return mat;
}

void matrix_copy_col_to_col(matrix* src,
							int from_row,
							int to_row,
							int src_col,
							matrix* dst,
							int dst_col)
{
	for (int i = from_row; i < to_row; i++)
	{
		dst->v[i][dst_col] = src->v[i][src_col];
	}
}

void matrix_copy_row_to_row(matrix* src,
							int from_col,
							int to_col,
							int src_row,
							matrix* dst,
							int dst_row)
{
	for (int j = from_col; j < to_col; j++)
	{
		dst->v[dst_row][j] = src->v[src_row][j];
	}
}

double matrix_col_norm(matrix* mat, int col)
{
	double sum = 0;

	for (int i = 0; i < mat->m; i++)
	{
		sum += mat->v[i][col] * mat->v[i][col];
	}

	return sqrt(sum);
}

matrix* matrix_div_scalar(matrix* mat, double k)
{
	matrix* result = matrix_new(mat->m, mat->n);

	for (int i = 0; i < mat->m; i++)
	{
		for (int j = 0; j < mat ->n; j++)
		{
			result->v[i][j] = mat->v[i][j] / k;
		}
	}

	return result;
}

void matrix_zero_cols(matrix* mat, int row, int col_start, int col_end)
{
	for (int j = col_start; j < col_end; j++)
	{
		mat->v[row][j] = 0.0;
	}
}

matrix* matrix_tridiag(matrix* A)
{
	A = matrix_copy(A);
	// Get size of the input matrix:
	int s = matrix_length(A);

	matrix* vector;

	// Get Start with iteration process from 2 to s-1
	for (int p = 1; p < s - 1; p++)
	{
		vector = matrix_new(s, 1);
		matrix_copy_col_to_col(A, p, s, p - 1, vector, 0);

		//vector = vector/norm(vector,2)
		vector = matrix_div_scalar(vector, matrix_col_norm(vector, 0));

		if (matrix_get(vector, p, 0) < 0)
		{
			vector = matrix_mul_scalar(vector, -1.0);
		}

		matrix_set(vector, p, 0, matrix_get(vector, p, 0) + 1.0);

		double alpha = matrix_get(vector, p, 0) * -1.0;

		matrix* Q = matrix_div_scalar(matrix_mul(A, vector), alpha);

		matrix* Q_transpose = matrix_transpose(Q);

		//beta = Q'*vector / (2*alpha)
		matrix* beta = matrix_div_scalar(matrix_mul(Q_transpose, vector), 2.0 * alpha);

		//Q = Q + (beta*vector)
		Q = matrix_add(Q, matrix_mul(vector, beta));
		Q_transpose = matrix_transpose(Q);

		//A = A + vector*Q' + Q*vector'
		matrix* vector_transpose = matrix_transpose(vector);
		A = matrix_add3(A,
			matrix_mul(vector, Q_transpose),
			matrix_mul(Q, vector_transpose));

		matrix* zeros = matrix_new(1, s - p);
		matrix_copy_row_to_row(zeros,
			p + 1,
			s,
			0,
			A,
			p - 1);

	}

	return A;
}