#ifndef _MATRIX_H_
#define _MATRIX_H_

typedef struct {
	int m, n;
	double** v;
} matrix;

matrix* matrix_new(int m, int n);
void matrix_delete(matrix* m);
matrix* matrix_transpose(matrix* m);
double matrix_get(matrix* mat,int m, int n);
void matrix_set(matrix* mat,int m, int n, double value);
int matrix_length(matrix* mat);
matrix* matrix_subtract(matrix* mat1, matrix* mat2);
matrix* matrix_add(matrix* mat1, matrix* mat2);
matrix* matrix_add3(matrix* mat1, matrix* mat2, matrix* mat3);
matrix* matrix_copy(matrix* mat);
matrix* matrix_mul(matrix* x, matrix* y);
matrix* matrix_slice(matrix* m, int slice_m, int slice_n);
void matrix_print(matrix* m);
void matrix_hessenberg(matrix* m);
void matrix_qr_factorization(matrix* A, matrix* Q, matrix* R);
int matrix_is_zero(matrix* mat);
matrix* matrix_mul_scalar(matrix* mat, double k);
matrix* matrix_eye(int m);
void matrix_copy_col_to_col(matrix* src,
							int from_row, 
							int to_row,
							int src_col, 
							matrix* dst, 
							int dst_col);

void matrix_copy_row_to_row(matrix* src,
							int from_col,
							int to_col,
							int src_row,
							matrix* dst,
							int dst_row);

double matrix_col_norm(matrix* mat, int col);
matrix* matrix_div_scalar(matrix* mat, double k);
void matrix_zero_cols(matrix* m, int row, int col_start, int col_end);
matrix* matrix_tridiag(matrix* matrix);

#endif