#include <cstdio>
#include <cstdlib>
#include <math.h>

#include "matrix.h"
#include "double_vect.h"

#include "QR_algorithm.h"

#define QR_ALGORITHM_TOLERANCE		0.00000000001

int QR_algorithm(matrix* A, double_vect*& out_eigenvalues, double_vect*& out_convergence_measure)
{
	//First, tridiagonalize the input matrix to a Hessenberg matrix.
	matrix* T = matrix_copy(A);
	matrix_hessenberg(T);

	int m = T->m;
	int n = T->n;

	if (n != m)
	{
		return QR_ERROR_MATRIX_NOT_SQUARE;
	}

	double_vect* convergence_measure = double_vect_new();
	double_vect* eigenvalues = double_vect_new();

	n -= 1;

	while (n > 0)
	{
		if (matrix_is_zero(T))
		{
			break;
		}

		// Perform QR decomposition.
		matrix* Q = matrix_new(T->m, T->n);
		matrix* R = matrix_new(T->m, T->n);

		matrix_qr_factorization(T, Q, R);

		// Multiply R and Q.
		T = matrix_mul(R, Q);

		// Add convergence information and extract eigenvalue if close enough.
		double cm = fabs(matrix_get(T, n, n - 1));
		double_vect_append(convergence_measure, cm);

		if (cm < QR_ALGORITHM_TOLERANCE)
		{
			for (int k = 0; k <= n; k++)
			{
				double ev = matrix_get(T, k, k);
				double_vect_insert(eigenvalues, k, ev);
			}

			matrix* temp = T;
			T = matrix_slice(T, n, n);
			matrix_delete(temp);

			n--;
		}

		matrix_delete(Q);
		matrix_delete(R);
	}

	out_eigenvalues = eigenvalues;
	out_convergence_measure = convergence_measure;

	return QR_SUCCESS;
}

int QR_algorithm_shift(matrix* A, double_vect*& out_eigenvalues, double_vect*& out_convergence_measure)
{
	// First, tridiagonalize the input matrix to a Hessenberg matrix.
	matrix* T = matrix_copy(A);
	matrix_hessenberg(T);

	int m = T->m;
	int n = T->n;

	if (m != n)
	{
		return QR_ERROR_MATRIX_NOT_SQUARE;
	}

	double_vect* convergence_measure = double_vect_new();
	double_vect* eigenvalues = double_vect_new();

	n -= 1;
	matrix* temp;

	while (n > 0)
	{
		if (matrix_is_zero(T))
		{
			break;
		}

		// Obtain the shift from the lower right corner of the matrix.
		matrix* eyemat = matrix_eye(T->m);
		matrix* mu_matrix = matrix_mul_scalar(eyemat, T->v[n][n]);
		matrix* shifted = matrix_subtract(T, mu_matrix);

		matrix* Q = matrix_new(T->m, T->n);
		matrix* R = matrix_new(T->m, T->n);

		// Perform QR decomposition on the shifted matrix.
		matrix_qr_factorization(shifted, Q, R);

		// Multiply R and Q and shift the matrix back.
		temp = T;
		T = matrix_add(matrix_mul(R, Q), mu_matrix);
		matrix_delete(temp);

		// Add convergence information and extract eigenvalue if close enough.
		double cm = fabs(matrix_get(T, n, n - 1));
		double_vect_append(convergence_measure, cm);

		if (cm < QR_ALGORITHM_TOLERANCE)
		{
			for (int k = 0; k <= n; k++)
			{
				double ev = matrix_get(T, k, k);
				double_vect_insert(eigenvalues, k, ev);
			}

			temp = T;
			T = matrix_slice(T, n, n);
			matrix_delete(temp);

			n--;

			matrix_delete(Q);
			matrix_delete(R);
		}

		out_eigenvalues = eigenvalues;
		out_convergence_measure = convergence_measure;
	}

	return QR_SUCCESS;
}