#include <cstdio>
#include <cstdlib>
#include <math.h>

#include "double_vect.h"
#include "matrix.h"
#include "QR_algorithm.h"

void Hessenberg_Test()
{
#define N		3

	matrix* A = matrix_new(N, N);

	A->v[0][0] = -149;
	A->v[0][1] = -50;
	A->v[0][2] = -154;

	A->v[1][0] = 537;
	A->v[1][1] = 180;
	A->v[1][2] = 546;

	A->v[2][0] = -27;
	A->v[2][1] = -9;
	A->v[2][2] = -25;

	puts("Original Matrix = \n");
	matrix_print(A);

	matrix_hessenberg(A);
	puts("\n");

	puts("Matrix in Hessenberg form = \n");
	matrix_print(A);

	getchar();	//so that the console window doesn't close immedialtely
}


int QR_Factorisation_Test()
{
#define N		3

	matrix* x = matrix_new(N, N);

	double in[N][N] =
	{
		{ 12, -51,   4},
		{  6, 167, -68},
		{ -4,  24, -41},
	};

	for (int i = 0; i < x->m; i++)
		for (int j = 0; j < x->n; j++)
			x->v[i][j] = in[i][j];


	puts("Original Matrix = ");
	matrix_print(x);

	matrix* Q = matrix_new(x->m, x->n);
	matrix* R = matrix_new(x->m, x->n);

	matrix_qr_factorization(x, Q, R);

	puts("Q = ");
	matrix_print(Q);

	puts("R = ");
	matrix_print(R);

	// to show their product is the input matrix
	matrix* m = matrix_mul(Q, R);
	puts("Q * R = ");
	matrix_print(m);

	matrix_delete(x);
	matrix_delete(R);
	matrix_delete(Q);
	matrix_delete(m);
	return 0;
}
