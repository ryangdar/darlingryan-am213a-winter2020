#ifndef _FUNCTION_TESTS_H_
#define _FUNCTION_TESTS_H_

/*
	This function tests the Hessenberg_Form_Orthogonal() functions against an
	example from Matlab website:
	https://www.mathworks.com/help/matlab/ref/hess.html
*/
void Hessenberg_Test();
/*
*	This function tests the matrix_qr_factorisation() function
*	by taking the QR factorization of a 3x3 matrix, then multiplying the
*	Q and R matrices to get back the original input matrix
*/
int QR_Factorisation_Test();

#endif