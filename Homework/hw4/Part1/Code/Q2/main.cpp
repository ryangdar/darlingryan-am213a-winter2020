#include <cstdio>
#include <cstdlib>
#include <math.h>

#include "matrix.h"
#include "double_vect.h"
#include "QR_algorithm_test.h"

#define N	3

int main()
{
	QR_algorithm_test();
	
	puts("\n");

	QR_algorithm_shift_test();

	getchar();
	return 0;
}

