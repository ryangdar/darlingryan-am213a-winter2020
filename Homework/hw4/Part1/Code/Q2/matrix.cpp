#include "matrix.h"

#include <cstdio>
#include <cstdlib>
#include <math.h>

#include "hessenberg_orthog.h"

matrix* matrix_new(int m, int n)
{
	matrix* x = (matrix*)malloc(sizeof(matrix));

	x->v = (double**)malloc(sizeof(double*) * m);
	x->v[0] = (double*)calloc(sizeof(double), m * n);
	for (int i = 0; i < m; i++)
		x->v[i] = x->v[0] + n * i;

	x->m = m;
	x->n = n;
	return x;
}

void matrix_delete(matrix* m)
{
	free(m->v[0]);
	free(m->v);
	free(m);
}

void matrix_transpose(matrix* m)
{
	for (int i = 0; i < m->m; i++) 
	{
		for (int j = 0; j < i; j++) 
		{
			double t = m->v[i][j];
			m->v[i][j] = m->v[j][i];
			m->v[j][i] = t;
		}
	}
}

double matrix_get(matrix* mat, int m, int n)
{
	return mat->v[m][n];
}

matrix* matrix_add(matrix* mat1, matrix* mat2)
{
	matrix* result = matrix_new(mat1->m, mat1->n);
	for (int i = 0; i < result->m; i++)
	{
		for (int j = 0; j < result->n; j++)
		{
			result->v[i][j] = mat1->v[i][j] + mat2->v[i][j];
		}
	}

	return result;
}


matrix* matrix_subtract(matrix* mat1, matrix* mat2)
{
	matrix* result = matrix_new(mat1->m, mat1->n);
	for (int i = 0; i < result->m; i++)
	{
		for (int j = 0; j < result->n; j++)
		{
			result->v[i][j] = mat1->v[i][j] - mat2->v[i][j];
		}
	}

	return result;
}

matrix* matrix_copy(matrix* mat)
{
	matrix* x = matrix_new(mat->m, mat->n);

	for (int i = 0; i < mat->m; i++)
	{
		for (int j = 0; j < mat->n; j++)
		{
			x->v[i][j] = mat->v[i][j];
		}
	}
	
	return x;
}

matrix* matrix_slice(matrix* mat, int slice_m, int slice_n)
{
	matrix *slice = matrix_new(slice_m, slice_n);
	for (int i = 0; i < slice->m; i++)
	{
		for (int j = 0; j < slice->n; j++)
		{
			slice->v[i][j];
		}
	}

	return slice;
}

matrix* matrix_mul(matrix* x, matrix* y)
{
	if (x->n != y->m)
		return NULL;

	matrix* r = matrix_new(x->m, y->n);

	for (int i = 0; i < x->m; i++)
		for (int j = 0; j < y->n; j++)
			for (int k = 0; k < x->n; k++)
				r->v[i][j] += x->v[i][k] * y->v[k][j];

	return r;
}

void matrix_print(matrix* m)
{
	for (int i = 0; i < m->m; i++) {
		for (int j = 0; j < m->n; j++) {
			printf(" %8.3f", m->v[i][j]);
		}
		printf("\n");
	}
	printf("\n");
}

void matrix_hessenberg(matrix* mat)
{
	matrix* u = matrix_new(mat->m, mat->n);
	Hessenberg_Form_Orthogonal(mat->v[0], u->v[0], mat->m);
}

static void matrix_copy_column(matrix* src, int srcColIndex, matrix* dst, int dstColIndex)
{
	for (int i = 0; i < dst->m; i++)
	{
		dst -> v[i][dstColIndex] = src -> v[i][srcColIndex];
	}
}

static void matrix_column_subtract(matrix* m1, int c1, matrix* m2, int c2)
{
	for (int i = 0; i < m1->m; i++)
	{
		m1->v[i][c1] -= m2->v[i][c2];
	}
}

/* Multiplies the matrix column c in m by k */
static matrix* matrix_column_multiply(matrix* m, int c, double k)
{
	for (int i = 0; i < m->m; i++) 
	{
		m->v[i][c] *= k;
	}
	
	return m;
}

/* Returns the length of the vector column in m */
static double vector_length(matrix* m, int column) 
{
	double length = 0;
	for (int row = 0; row < m->m; row++) 
	{
		length += m->v[row][column] * m->v[row][column];
	}

	return sqrt(length);
}

/* Divides the matrix column c in m by k */
static matrix* matrix_column_divide(matrix* m, int c, double k) 
{
	for (int i = 0; i < m->m; i++) 
	{
		m->v[i][c] /= k;
	}

	return m;
}

void matrix_qr_factorization(matrix* A, matrix* Q, matrix* R)
{
	matrix* T = matrix_new(A->m, 1);
	matrix* S = matrix_new(A->m, 1);

	for (int i = 0; i < A->n; i++) 
	{

		matrix_copy_column(A, i, Q, i);

		for (int j = 0; j < i; j++) {

			matrix_copy_column(Q, j, T, 0);
			matrix_copy_column(A, i, S, 0);

			double r = 0;
			
			for (int k = 0; k < A->m; k++) 
			{
				r += T->v[k][0] * S->v[k][0];
			}

			R->v[j][i] = r;
			matrix_column_subtract(Q, i, matrix_column_multiply(T, 0, r), 0);

		}

		R->v[i][i] = vector_length(Q, i);
		matrix_column_divide(Q, i, R->v[i][i]);

	}

}

int matrix_is_zero(matrix* mat)
{
	for (int i = 0; i < mat->m; i++)
	{
		for (int j = 0; j < mat->n; j++)
		{
			if (fabs(mat->v[i][j] > 0.000000000000001))
			{
				return 0;
			}
		}
	}

	return 1;
}

matrix* matrix_mul_scalar(matrix* mat, double k)
{
	matrix* result = matrix_new(mat->m, mat->n);

	for (int i = 0; i < mat->m; i++)
	{
		for (int j = 0; j < mat->n; j++)
		{
			result->v[i][j] = mat->v[i][j] * k;
		}
	}

	return result;
}

matrix* matrix_eye(int m)
{
	matrix* mat = matrix_new(m, m);
	for (int i = 0; i < m; i++)
	{
		for (int j = 0; j < m; j++)
		{
			if (i == j)
			{
				mat->v[i][j] = 1;
			}
			else
			{
				mat->v[i][j] = 0;
			}
		}
	}

	return mat;
}