#ifndef _MATRIX_H_
#define _MATRIX_H_

typedef struct {
	int m, n;
	double** v;
} matrix;

matrix* matrix_new(int m, int n);
void matrix_delete(matrix* m);
void matrix_transpose(matrix* m);
double matrix_get(matrix* mat,int m, int n);
matrix* matrix_subtract(matrix* mat1, matrix* mat2);
matrix* matrix_add(matrix* mat1, matrix* mat2);
matrix* matrix_copy(matrix* mat);
matrix* matrix_mul(matrix* x, matrix* y);
matrix* matrix_slice(matrix* m, int slice_m, int slice_n);
void matrix_print(matrix* m);
void matrix_hessenberg(matrix* m);
void matrix_qr_factorization(matrix* A, matrix* Q, matrix* R);
int matrix_is_zero(matrix* mat);
matrix* matrix_mul_scalar(matrix* mat, double k);
matrix* matrix_eye(int m);

#endif