#ifndef _DOUBLE_VECT_H_
#define _DOUBLE_VECT_H_

typedef struct {
	double* values;
	int length;
}double_vect;

double_vect* double_vect_new();
double double_vect_append(double_vect* vect, double value);
double double_vect_get(double_vect* vect, int index);
void double_vect_insert(double_vect* vect, int index, double value);


#endif