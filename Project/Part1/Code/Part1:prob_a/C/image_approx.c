#include "matrix.h"
#include "image_approx.h"

matrix* approximate_image(int m, int n, matrix* u, matrix* s, matrix* v, int k)
{
	matrix* s2 = matrix_copy(s);

	for (int i = k; i < s2->m; i++)
	{
		matrix_set(s2, i, i, 0);
	}

	matrix* a1 = matrix_mul(u, s2);
	matrix* ans = matrix_mul(a1, matrix_transpose(v));
	matrix_delete(a1);
	matrix_delete(s2);
	return ans;
}

matrix* approximate_image2(int m, int n, matrix* u, matrix* s, matrix* v, int k)
{
	matrix* B = matrix_new(m, n);
	matrix* A_hat = matrix_new(m, n);

	for (int i = 0; i < k; i++)
	{
		matrix* outer = matrix_outer_product(matrix_get_col(u, i), matrix_get_col(v, i));
		matrix_mul_scalar_inplace(outer, matrix_get(s, i, i));
		matrix_add_inplace(B, outer);
	}

	for (int i = 0; i < m; i++)
	{
		for (int j = 0; j < n; j++)
		{
			double v = matrix_get(B, i, j);
			if (v > 255)
			{
				v = 255;
			}
			else if (v < 0)
			{
				v = 0;
			}

			matrix_set(A_hat, i, j, v);
		}
	}

	return A_hat;
}

