#ifndef _IMAGE_APPROX_H_
#define _IMAGE_APPROX_H_

matrix* approximate_image2(int m, int n, matrix* u, matrix* s, matrix* v, int k);
matrix* approximate_image(int m, int n, matrix* u, matrix* s, matrix* v, int k);

#endif