#include <stdio.h>

#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "matrix.h"
#include "double_vect.h"

#include "svd.h"
#include "image_approx.h"


int main(int argc, char** argv)
{
	if (argv == NULL)
	{
		puts("ERROR: No command line arguments");
		return -1;
	}

	if (argv[1] == NULL)
	{
		puts("ERROR: Input file not passed as arguments");
		return -1;
	}

	if (argv[2] == NULL)
	{
		puts("ERROR: Output file not passed as arguments");
		return -1;
	}

	if (argv[3] == NULL)
	{
		puts("ERROR: Not value for k is passed");
		return -1;
	}

	char* input_image_file_path = argv[1];		//relative to the exe file
	char* output_file_path = argv[2];		//relative to the exe file
	int k = atoi(argv[3]);											//the reconstruction factor
	

	puts("Reading file into matrix");
	byte transposed;
	matrix* A = matrix_from_datfile(input_image_file_path, &transposed);

	printf("Matrix dimentions = %ix%i\n", A->m, A->n);

	matrix* V = NULL, * S = NULL, * U = NULL;

	puts("Performing SVD, this may take a while.");
	int failed = matrix_svd(A, &U, &S, &V);

	if (failed)
	{
		puts("Failed to perform SVD on the input image.");
	}
	else
	{
		puts("SVD done Successfully");

		matrix* S_diag = matrix_to_diagonal(S);
		matrix_delete(S);

		printf("Reconstructing approximate matrix for k = %i\n", k);
		matrix* ap = approximate_image(A->m, A->n, U, S_diag, V, k);
		
		puts("Creating byte matrix");
		matrix* byte_matrix = matrix_as_byte(ap);

		matrix_delete(ap);
		matrix_delete(A);
		matrix_delete(U);
		matrix_delete(S_diag);

		printf("Writing output file '%s'. Please wait......", output_file_path);

		if (transposed)
		{
			matrix* temp = byte_matrix;
			byte_matrix = matrix_transpose(byte_matrix);
			matrix_delete(temp);
		}

		matrix_to_datfile(output_file_path, byte_matrix);

		puts("\n\nAll Done.");
	}

	getchar();
	return 0;
}
