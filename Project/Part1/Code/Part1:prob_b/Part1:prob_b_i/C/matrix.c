//Takes in the diagonals of the matrix and tolerance level. It then ouputs the resulting matrix X and the error at each iteration. 

#include<stdio.h>
#include<math.h>
#define X 10
int main()
{
	FILE *fptr ;
    int n = 10, i, j, k, method_type, diag_num, flag = 0, cnt = 0;
    float a[n][n+1];            //declare a 2d array for storing the elements of the augmented matrix
    float x[n];                //declare an array to store the values of variables
    float eps, val;
    float real_eps;
    
    int count, t, limit = 10;
    float temp, error, _a, sum = 0; 
    float matrix[20][20], y[20], allowed_error;
    fptr = fopen("error.dat", "w") ;
    
    printf("\nEnter the type of algorithum you want to use:\t");
	printf("\nIf you want to use Seidel method, type 1:\t");
	printf("\nIf you want to use Jacobi method, type 2:\t");
	scanf("%d", &method_type);
	printf("\nEnter the first expected number of Diagonals:\t");
	scanf("%d", &diag_num);
	printf("Enter Allowed Error:\t");
	scanf("%f", &allowed_error);
	
	if(method_type == 1) {

		eps = allowed_error;

		for(i = 0; i < n; i++)
	  	{
	        for(j = 0; j <= n; j++)
	        {
	        	if(i == j) {
	        		a[i][j] = diag_num;
				} else if (j == n ) {
					a[i][j] = i + 1;
				} else {
					a[i][j] = 1;	
				}
	        }
	
	  	}
	  
	    for (i = 0; i < n; i++)
	    	x[i] = 0;					//  Set the initial Value...
	    
	    for (i = 0; i < n; i++)                    //Pivotisation(partial) to make the equations diagonally dominant
	        for (k = i + 1; k < n; k++)
	            if (fabs(a[i][i]) < fabs(a[k][i]))
	                for (j = 0; j <= n; j++)
	                {
	                    double temp = a[i][j];
	                    a[i][j] = a[k][j];
	                    a[k][j] = temp;
	                }
	  	printf("\n-------------------------------------------------------------------\n");
	  	
	    do
	    {
	        for (i = 0; i < n; i++)                //Loop that calculates x1,x2,...xn
	        {
	            val = x[i];
	            x[i] = a[i][n];
	            for(j = 0; j < n; j++)
	            {
	                if (j != i)
	                	x[i] = x[i] - a[i][j] * x[j];
	            }
	            x[i] = x[i] / a[i][i];
	            if (fabs(x[i] - val) <= eps)           //Compare the ne value with the last value
				{
					real_eps = fabs(x[i] - val);
					flag++;
				}
				fprintf(fptr, "%f\n", fabs(x[i] - val));
	        }
	        cnt++;
	        
	    } while(flag < n);                        //If the values of all the variables don't differ from their previious values with error more than eps then flag must be n and hence stop the loop
	    
	    printf("\n The solution is as follows:\n");
	    
	    for (i = 0; i < n; i++)
	        printf("\nX[%d]:\t%f", i, x[i]);        //Print the contents of x[]
	    
		printf("\n");
		printf("\n");
	    printf("\nError for Seidel Method :\t%f", real_eps);
	    return 0;
    
	    
	} else if (method_type == 2) {

	   	for(count = 1; count <= limit; count++)
	   	{
            for(t = 1; t <= limit + 1; t++)
            {
            	if(count == t) {
            		matrix[count][t] = diag_num;
				} else if (t == (limit + 1)) {
					matrix[count][t] = count;
				} else {
					matrix[count][t] = 1;	
				}
            }
	
	   	}
	  
	   	for(count = 1; count <= limit; count++)
	   	{
	       y[count] = 0;
	   	}
	      	do
	      	{
	            _a = 0;
	            for(count = 1; count <= limit; count++)
	            {
	               	sum = 0;
	               	for(t = 1; t <= limit; t++)
	               	{
                        if(t != count)
                        {
                              sum = sum + matrix[count][t] * y[t];
                        }
	               	}
	               	temp = (matrix[count][limit + 1] - sum) / matrix[count][count];
	               	error = fabs(y[count] - temp);
	               	if(error > _a)
	               	{
	                   _a = error;
	               	}
	               	y[count] = temp;
	               	fprintf(fptr, "%f\n", error);
	            }
	      	}
	      	while(_a >= allowed_error);
	      	printf("\n\nSolution\n\n");
	      	for(count = 1; count <= limit; count++)
	      	{
	            printf("\nX[%d]:\t%f", count, y[count]);
	      	}
	      	printf("\n");
	      	printf("\n");
	      	printf("\nError for Jacobi Method :\t%f", _a);
	   		return 0;
	}
	
}
