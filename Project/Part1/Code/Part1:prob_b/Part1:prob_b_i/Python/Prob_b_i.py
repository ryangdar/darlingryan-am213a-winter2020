#!/usr/bin/env python
# coding: utf-8

# In[1]:


# The values in my dataset are derived from running the terminal function for specified values of D


# # Imports

# In[2]:


import pandas as pd
import plotly
import plotly.express as px
import plotly.graph_objects as go


# In[3]:


data1 = np.loadtxt('../C/Jacobi_2d.dat' )
data2 = np.loadtxt('../C/Jacobi_5d.dat' )
data3 = np.loadtxt('../C/Jacobi_10d.dat' )
data4 = np.loadtxt('../C/Jacobi_100d.dat' )
data5 = np.loadtxt('../C/Jacobi_1000d.dat' )
data6 = np.loadtxt('../C/Seidel_2d.dat' )
data7 = np.loadtxt('../C/Seidel_5d.dat' )
data8 = np.loadtxt('../C/Seidel_10d.dat' )
data9 = np.loadtxt('../C/Seidel_100d.dat' )
data10 = np.loadtxt('../C/Seidel_1000d.dat' )


# # Graphs

# In[4]:


x1 = np.linspace(0, np.size(data1), np.size(data1), endpoint=False)
x2 = np.linspace(0, np.size(data2), np.size(data2), endpoint=False)
x3 = np.linspace(0, np.size(data3), np.size(data3), endpoint=False)
x4 = np.linspace(0, np.size(data4), np.size(data4), endpoint=False)
x5 = np.linspace(0, np.size(data5), np.size(data5), endpoint=False)
x6 = np.linspace(0, np.size(data6), np.size(data6), endpoint=False)
x7 = np.linspace(0, np.size(data7), np.size(data7), endpoint=False)
x8 = np.linspace(0, np.size(data8), np.size(data8), endpoint=False)
x9 = np.linspace(0, np.size(data9), np.size(data9), endpoint=False)
x10 = np.linspace(0, np.size(data10), np.size(data10), endpoint=False)


# In[5]:


from plotly.subplots import make_subplots
import plotly.graph_objects as go

fig = make_subplots(rows=2, cols=5)

fig.add_trace(
    go.Scatter(
        x=x1,
        y=data1,
        mode="markers"
    ),
    row=1, col=1
)

fig.add_trace(
    go.Scatter(
        x=x2,
        y=data2,
        mode="markers"
    ),
    row=1, col=2
)

fig.add_trace(
    go.Scatter(
        x=x3,
        y=data3,
        mode="markers"
    ),
    row=1, col=3
)

fig.add_trace(
    go.Scatter(
        x=x4,
        y=data4,
        mode="markers"
    ),
    row=1, col=4
)

fig.add_trace(
    go.Scatter(
        x=x5,
        y=data5,
        mode="markers"
    ),
    row=1, col=5
)

fig.add_trace(
    go.Scatter(
        x=x6,
        y=data6,
        mode="markers"
    ),
    row=2, col=1
)

fig.add_trace(
    go.Scatter(
        x=x7,
        y=data7,
        mode="markers"
    ),
    row=2, col=2
)

fig.add_trace(
    go.Scatter(
        x=x8,
        y=data8,
        mode="markers"
    ),
    row=2, col=3
)

fig.add_trace(
    go.Scatter(
        x=x9,
        y=data9,
        mode="markers"
    ),
    row=2, col=4
)

fig.add_trace(
    go.Scatter(
        x=x10,
        y=data10,
        mode="markers"
    ),
    row=2, col=5
)


fig.update_layout(
    title="Iterations Vs Norm Error (First row: Seidel, Second Row: Jacobi)",
    yaxis_title="Norm Error",
    font=dict(
        family="Courier New, monospace",
        size=18,
        color="Black"
    )
)

# Update xaxis properties
fig.update_xaxes(title_text="D2", row=1, col=1)
fig.update_xaxes(title_text="D5", row=1, col=2)
fig.update_xaxes(title_text="D10", row=1, col=3)
fig.update_xaxes(title_text="D100", row=1, col=4)
fig.update_xaxes(title_text="D1000", row=1, col=5)
fig.update_xaxes(title_text="D2", row=2, col=1)
fig.update_xaxes(title_text="D5", row=2, col=2)
fig.update_xaxes(title_text="D10", row=2, col=3)
fig.update_xaxes(title_text="D100", row=2, col=4)
fig.update_xaxes(title_text="D1000", row=2, col=5)

plotly.offline.plot(fig, filename= "Seidel_Jacobi.html")


# In[ ]:




