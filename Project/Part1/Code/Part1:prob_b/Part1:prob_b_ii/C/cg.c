// Includes the GSL library to compute LU
// Takes in the dimensions, diagonals, and the vector b. It then outputs the resulting error at each iteration and matrix X.


#include <stdio.h>
#include <math.h>
#include <gsl/gsl_linalg.h>

int n;							 
FILE *fptr;

inline void axpy(double *dest, double a, double *x, double *y, int n)
{
	register int i;

	for (i = 0; i < n; ++i)
		dest[i] = a * x[i] + y[i];
}

inline double ddot(double *x, double *y, int n)
{
	double final_sum = 0;
	register int i;

	for (i = 0; i < n; ++i)
		final_sum += x[i] * y[i];

	return final_sum;
}

inline void blas_dgemv(double *A, double *s, double *z, int nn)
{
	register int i, j;

	for (i = 0; i < nn; ++i)
	{
		z[i] = 0.0;

		for (j = 0; j < nn; ++j)
			z[i] += A[i * nn + j] * s[j];
	}

	return;
}

void LU_direct(double *A, double *b, double *x, int nn, int mes)
{
	int s = 0; 

	gsl_matrix_view m = gsl_matrix_view_array(A, nn, nn);
	gsl_vector_view bb = gsl_vector_view_array(b, nn);
	gsl_vector_view xx = gsl_vector_view_array(x, nn);
	gsl_permutation *p = gsl_permutation_calloc(nn);

	gsl_linalg_LU_decomp(&m.matrix, p, &s);

	gsl_linalg_LU_solve(&m.matrix, p, &bb.vector, &xx.vector);

	gsl_permutation_free(p);
	if (mes)
		printf("-- Solution is done.\n");
}

int CG_iterative(double *A, double *b, double *x, double *M, int max_iter,
				 double tol, int nn)
{
	fptr = fopen("error.dat", "w");
	double bnorm2;
	double rnorm2;
	double rz, rzold;
	double alpha, beta;
	double rz_local, rnorm2_local, bnorm2_local;
	double *s;
	double *r;
	double *z;
	double MM[nn * nn];
	register int i;
	register int it; 

	s = (double *)malloc(nn * sizeof(double));

	r = (double *)malloc(nn * sizeof(double));

	z = (double *)malloc(nn * sizeof(double));

	for (i = 0; i < nn * nn; ++i)
		MM[i] = M[i];

	bnorm2 = ddot(b, b, nn);

	for (i = 0; i < nn; ++i)
	{
		x[i] = 0.0;	 
		r[i] = b[i]; 
	}

	LU_direct(M, r, z, nn, 0);

	for (i = 0; i < nn * nn; ++i)
		M[i] = MM[i];

	for (i = 0; i < nn; ++i)
		s[i] = z[i];

	rz = ddot(r, z, nn);
	rnorm2 = ddot(r, r, nn);

	for (it = 0; it < max_iter; ++it)
	{

		blas_dgemv(A, s, z, nn);

		alpha = rz / ddot(s, z, n);
		axpy(x, alpha, s, x, n);
		axpy(r, -alpha, z, r, n);

		LU_direct(M, r, z, nn, 0);

		for (i = 0; i < nn * nn; ++i)
			M[i] = MM[i];

		rzold = rz;

		rz = ddot(r, z, n);
		beta = -rz / rzold;
		axpy(s, -beta, s, z, n);

		rnorm2 = ddot(r, r, n);
		fprintf(fptr, "%lg\n", rnorm2);
		if (rnorm2 <= bnorm2 * tol * tol)
		{
			printf("-- Solution converged at iteration '%d' with residual norm of %f.\n", it, sqrt(rnorm2));
			fclose(fptr);
			break; 
		}
	}

	free(s);
	free(r);
	free(z);

	// the method did not converge after reaching maximum number of iterations
	if (it >= max_iter)
	{
		printf("-- Solution did not converge at the desired tolerance.\n");
		return -1;
	}

	return it;
}

int main(void)
{
	int iterator;

	double *m_data, *a_data, *b_data, *x_direct, *x_iterat;
	printf("Enter the size of matrix \n");
	scanf("%d", &n);
	a_data = (double *)malloc(sizeof(double) * n * n);
	m_data = (double *)malloc(sizeof(double) * n * n);
	b_data = (double *)malloc(sizeof(double) * n);
	x_direct = (double *)malloc(sizeof(double) * n);
	x_iterat = (double *)malloc(sizeof(double) * n);
	printf("Enter the diagonal elements\n");
	for (iterator = 0; iterator < n * n; iterator++)
	{
		a_data[iterator] = 1;
	}
	for (iterator = 0; iterator < n; iterator++)
	{
		scanf("%lg", &a_data[n * iterator + iterator]); 
	}

	for (iterator = 0; iterator < n * n; iterator++)
	{
		m_data[iterator] = 0;
	}
	for (iterator = 0; iterator < n; iterator++)
	{
		m_data[n * iterator + iterator] = 1; 
	}

	printf("\nEnter the value of b\n");
	for (iterator = 0; iterator < n; iterator++)
	{
		scanf("%lg", &b_data[iterator]);
	}

	for (iterator = 0; iterator < n; iterator++)
	{
		x_direct[iterator] = 0;
	}
	for (iterator = 0; iterator < n; iterator++)
	{
		x_iterat[iterator] = 0;
	}

	double test1[n];  
	double test2[n]; 
	double A[n * n]; 
	double tol = 1e-5; 
	int max_iter = n;  
	int iters = 0;
	register int i;

	for (i = 0; i < n * n; ++i)
		A[i] = a_data[i];

	for (i = 0; i < n * n; ++i)
		A[i] = a_data[i];

	iters = CG_iterative(A, b_data, x_iterat, m_data, max_iter, tol, n);
	printf("Iterative (Conjugate Gradient) solution results: \n");
	for (i = 0; i < n; ++i)
		printf("x[%d]: %f\n", i, x_iterat[i]);

	return 0;
}
